# README #

digital-to-graph.py is a script created by the ArsCyber trademark.
It allows converting digital signal to a graph.

### Description ###

A list of digital signal tuples (time, signal value) is converted to a graph. Each node is the value of the signal, and each edge is the timing which separates the signals. Each edge has a specific weight, which represents the timing.
The output format is a list of unique edges and their associated weights. If more than one weight is present, there is more than one edge.

### Uses ###

This script can be used to transform digital or analog signal, including waves, into a graph (analog signal and waves have to be transformed to digital signal first). 
This means that it can be used to build hypergraphs and multilayered hypergraphs for advanced Cognitive Computing systems which allow parallel signals. 
An example is an expert system which allows for advanced speech tone inflection. But there are far more potential uses.