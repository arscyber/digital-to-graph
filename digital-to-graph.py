"""
digital-to-graph.py

Digital signal to graph transformation software.


ArsCyber™ Software Trademark
© Copyright
All rights reserved.

This software is in its early alpha stage.
"""


graph = []
weights = {}
signal = []
signal = [(1, 100), (3, 200), (4, 200), (5, 200), (7, 200)] #Sample values. The digital signal is provided through a form of tuples. The first value is the time of the signal, and the second value is the value of the signal.


def tupleArray(tupleValue):
    for v in graph:
        if isinstance(v, list):
            if tupleValue in v:
                return v
                break
            
    return None


def digitalToGraph():
    
    for i in range(0, len(signal)-1):

        if tupleArray((signal[i][1], signal[i+1][1])) is None:

            if (signal[i][1], signal[i+1][1]) in graph:
                graph.remove((signal[i][1], signal[i+1][1]))
                graph.append([(signal[i][1], signal[i+1][1]), (signal[i][1], signal[i+1][1])])
            else:
                graph.append((signal[i][1], signal[i+1][1]))
            
        else:

            tupleArrayBuffer = tupleArray((signal[i][1], signal[i+1][1]))
            graph.remove(tupleArray((signal[i][1], signal[i+1][1])))
            tupleArrayBuffer.append((signal[i][1], signal[i+1][1]))
            graph.append(tupleArrayBuffer)

    for i in range(0, len(signal)-1):
        for v in graph:
            if isinstance(v, list):
                if v[0] in weights:
                    weights[v[0]].append(signal[i+1][0]-signal[i][0])
                else:
                    weights[v[0]] = [signal[i+1][0]-signal[i][0]]
            else:
                if tupleArray(v) is None and v not in weights:
                    weights[v] = [signal[i+1][0]-signal[i][0]]

     
def main():

    digitalToGraph()

    print(graph)
    print(weights)


if __name__ == "__main__":
    main()
